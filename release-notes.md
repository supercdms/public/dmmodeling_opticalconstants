# Optical Constants Repository - Release Notes
Contact: Matt Wilson (matthew.wilson@kit.edu)

Record of releases and tags for the Optical Constants repository. The tagging procedure follows the [Semantic Versioning](https://semver.org/) method.

---
## v1.2.9
**Date:** September 2, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed bug in setup file in the get_package_version_hist function. In some cases, git url could not be resolved.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v1.2.8
**Date:** September 1, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed a bug in the setup file that was causing automatic installation of the dmmodeling_physicsvalues package even if the package with the correct version existed. The but has now been fixed.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v1.2.7
**Date:** September 1, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated README and installation instructions based on the new name of the public version of this package, as well as public urls to dependent packages. Updated names and urls in setup file.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v1.2.6
**Date:** August 15, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated get_package_version_hist function in setup file to avoid installing tagged versions that do not begin with 'v'.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v1.2.5
**Date:** August 15, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updates based on ORC review process for this software package.

**Changes from Last Release Version:** Fixed typos in README and added additional information. Added statement in LICENSE. The 'temp' input argument has been renamed to 'temperature'. 

---


## v1.2.4
**Date:** April 26, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated Issue Tracking section in README. Added statement in README about naming convention of photoelectric absorption cross section. Updated url of physicsvalues package in setup.py file.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v1.2.3
**Date:** Feb 13, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Bug found with checks to input parameters. Line breaks were added but some string conversions "%s" were not placed correctly. Bug fixed.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v1.2.2
**Date:** Feb 11, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Added sentence in README that this package is used by the SuperCDMS collaboration.

**Changes from Last Release Version:** No change to the functionality from previous version.

---

## v1.2.1
**Date:** Feb 9, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Minor edits to doc string. Minor edits to the checks on the input parameters. Updated dependence on physicsvalues package to v2.2.0. Minor updates to README.

**Changes from Last Release Version:** No change to the functions available in this package.

---

## v1.2.0
**Date:** Nov 28, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Changed how the error propagation is done for the indirect absorption model in Si. Now it is using the standard error propagation with covariance matrix and partial derivative. Package requires physicsvalues >= 2.1.3, which includes the updated parameter values for the indirect absorption model in Si. The setup file has a lot of new custom functions to enforce install requirements for customs packages (i.e. physicsvalues). Version of package now defined in \_version.py file. 

**Changes from Last Release Version:** Indirect Absorption Model in Si using updated fit parameter values. Indirect Absorption Model in Si using updated error propgagation method. Package installation now include requirement on the physicsvalues custom package. Package version now defined in the \_version.py file. Updated README.

---

## v1.1.1
**Date:** Oct 26, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Changed install_requirements in setup.py file. Instead of looking for dependency_links with site.USER_SITE, it will look to see if the user has the required package with the correct git tag.

**Changes from Last Release Version:** No changes to functionality. Minor change to installation.

---

## v1.1.0
**Date:** Oct 25, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Removed hard-coded constants and parameters and replaced with the values loaded from the physicsvalues package. Replaced data stored in txt files with data stored in xml files. Updated loader function to read in the data from the xml files. Restructured repository to it can by installed as a python package.

**Changes from Last Release Version:** No changes to the tool functions. Repository can now be installed as a python package. txt files now replaced with xml files. No hard-coded constants/parameters.

---

## v1.0.2
**Date:** June 16, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** In the optical_constants_tools.py module, added the local directory so that datafiles can be loaded with paths relative to the location of the module.

**Changes from Last Release Version:** No changes to tool functions, but datafiles can now be added without error.

---

## v1.0.1
**Date:** May 19, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixing some bugs with loading the tool. Removed the .ipynb_checkpoint folders which should never have been there. Added a `__init__.py` file.

**Changes from Last Release Version:** Not changes to tool functions.

---

## v1.0.0
**Date:** May 12, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** First commit to repository. Committed the `optical_constants_tools.py` file and the `README.md` file, as well as several data. The following tools are available in `optical_constants_tools.py`:
-  `grab_pexsec_Si` (and several downstream function): used to extract the photoelectric cross section values for silicon AND the real part of the complex conductivity values for silicon.
- `grab_conductivity_imaginary`: used to extract the imaginary part of the complex conductivity values for silicon AND germanium.

Furthermore, several datafiles were included in this commit:
- Photoelectric absorption cross section data for Si from the article [Silicon (Si)](https://doi.org/10.1016/B978-012544415-6.50027-3) by D. Edwards found in the Handbook of Optical Constants, [X-Ray Interactions: Photoabsorption, Scattering, Transmission, and Reflection at E = 50-30,000 eV, Z = 1-92](https://doi.org/10.1006/adnd.1993.1013) by Henke *et. al*., and [NIST XCOM database (version 1.5)](https://dx.doi.org/10.18434/T48G6X) by Berger *et. al*.
- Index of refraction data for Si from article [Silicon (Si)](https://doi.org/10.1016/B978-012544415-6.50027-3) by D. Edwards found in the Handbook of Optical Constants.
- Imaginary part of the complex conductivity data for Si and Ge from [Absorption of light dark matter in semiconductors](https://doi.org/10.1103/PhysRevD.95.023013) by Hochberg *et. al.*

**Changes from Last Release Version:** N/A
