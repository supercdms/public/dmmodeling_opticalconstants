## all function related to the indirect absorption model in Si

import numpy as np

__all__ = [
    "compute_indirect_absorption",
    "compute_indirect_absorption_uncertainty",
]

def unbox_indirectabsorption_Si_params(constants):
    """
    Simple function to easily grab the constants and parameters for the 
    indirect absorption model for Si.
    """
    
    rho_Si = constants.Si_params.Si_density.value*1e-3 # density of Si (g/cm^3)
    c = constants.physics_params.speed_of_light.value # speed of light (cm/s)
    h = constants.physics_params.planck.value # Planck constant (eVs)
    k = constants.physics_params.k.value # Boltzmann constant (eV K^-1)
    
    
    # unfitted parameters for the indirect absorption model
    gamma = constants.Si_params.bandgap_params.gamma.value # band gap energy temp dependence parameter (K)
    beta = constants.Si_params.bandgap_params.beta.value # band gap energy temp dependence parameter (eV/K)
    Eg20 = constants.Si_params.bandgap_params.Eg20.value # energy of second indirect band gap at 0K (eV)
    Egd0 = constants.Si_params.bandgap_params.Egd0.value # energy of direct band gap at 0K
    Ad = constants.Si_params.indirect_absorption_model_params.Ad.value # proportionality constant of direct band gap (eV^-1/2 cm^-1)
    C1 = constants.Si_params.indirect_absorption_model_params.C1.value # electron-phonon coupling constant for first phonon energy
    C2 = constants.Si_params.indirect_absorption_model_params.C2.value # electron-phonon coupling constant for second phonon energy
    Ep1 = constants.Si_params.indirect_absorption_model_params.Ep_TA.value # TA vibrational mode phonon energy (eV)
    Ep2 = constants.Si_params.indirect_absorption_model_params.Ep_TO.value # TO vibrational mode phonon energy (eV)
    
    # fitted parameters for the indirect absorption model
    c0 = constants.Si_params.indirect_absorption_model_params.c0.value # first indirect band gap proportionality constant parameter, nominal value (eV^-2 cm^-1)
    c1 = constants.Si_params.indirect_absorption_model_params.c1.value # first indirect band gap proportionality constant parameter, nominal value (K^-1)
    A2 = constants.Si_params.indirect_absorption_model_params.A2.value # second indirect band gap proportionality constant, nominal value (eV^-2 cm^-1)
    Eg10 = constants.Si_params.bandgap_params.Eg10.value # energy of first indirect band gap at 0K, nominal value (eV)
    
    return rho_Si, c, h, k, gamma, beta, Eg20, Egd0, Ad, C1, C2, Ep1, Ep2, c0, c1, A2, Eg10
    
def compute_dsigmadc0(energies,temperature,constants):
    """
    Function to compute the dsigma/dc0 for the indirect absorption model.
    """
    
    rho_Si, c, h, k, gamma, beta, Eg20, Egd0, Ad, C1, C2, Ep1, Ep2, c0, c1, A2, Eg10 = unbox_indirectabsorption_Si_params(constants)
    
    dsigmadc0 = np.zeros(shape=len(energies))
    
    # determine (derivative) A1 proportionality constant
    dA1 = np.exp(-c1*temperature)
    
    # determine the band gap energies from the given temperature
    Eg1T = Eg10 - ((beta*temperature*temperature)/(temperature + gamma))
    
    if temperature == 0:
        # first indirect band gap terms
        energy_mask = energies >= Eg1T - Ep1
        dsigmadc0[energy_mask] += 0
        
        energy_mask = energies >= Eg1T + Ep1
        dsigmadc0[energy_mask] += C1*dA1*np.power(energies[energy_mask] - Eg1T - Ep1,2.0)
        
        energy_mask = energies >= Eg1T - Ep2
        dsigmadc0[energy_mask] += 0
        
        energy_mask = energies >= Eg1T + Ep2
        dsigmadc0[energy_mask] += C2*dA1*np.power(energies[energy_mask] - Eg1T - Ep2,2.0)

    else:
        # first indirect band gap terms
        energy_mask = energies >= Eg1T - Ep1
        dsigmadc0[energy_mask] += C1*dA1*np.power(energies[energy_mask] - Eg1T + Ep1,2.0)/(np.exp(Ep1/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg1T + Ep1
        dsigmadc0[energy_mask] += C1*dA1*np.power(energies[energy_mask] - Eg1T - Ep1,2.0)/(1 - np.exp(-Ep1/(k*temperature)))
        
        energy_mask = energies >= Eg1T - Ep2
        dsigmadc0[energy_mask] += C2*dA1*np.power(energies[energy_mask] - Eg1T + Ep2,2.0)/(np.exp(Ep2/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg1T + Ep2
        dsigmadc0[energy_mask] += C2*dA1*np.power(energies[energy_mask] - Eg1T - Ep2,2.0)/(1 - np.exp(-Ep2/(k*temperature)))
    
    # convert to units of cm^2/g
    dsigmadc0 *= 1/rho_Si
    
    return dsigmadc0

def compute_dsigmadc1(energies,temperature,constants):
    """
    Function to compute the dsigma/dc1 for the indirect absorption model
    """
    
    rho_Si, c, h, k, gamma, beta, Eg20, Egd0, Ad, C1, C2, Ep1, Ep2, c0, c1, A2, Eg10 = unbox_indirectabsorption_Si_params(constants)
    
    dsigmadc1 = np.zeros(shape=len(energies))
    
    # determine (derivative) A1 proportionality constant
    dA1 = -temperature*c0*np.exp(-c1*temperature)
    
    # determine the band gap energies from the given temperature
    Eg1T = Eg10 - ((beta*temperature*temperature)/(temperature + gamma))
    
    if temperature == 0:
        # first indirect band gap terms
        energy_mask = energies >= Eg1T - Ep1
        dsigmadc1[energy_mask] += 0
        
        energy_mask = energies >= Eg1T + Ep1
        dsigmadc1[energy_mask] += C1*dA1*np.power(energies[energy_mask] - Eg1T - Ep1,2.0)
        
        energy_mask = energies >= Eg1T - Ep2
        dsigmadc1[energy_mask] += 0
        
        energy_mask = energies >= Eg1T + Ep2
        dsigmadc1[energy_mask] += C2*dA1*np.power(energies[energy_mask] - Eg1T - Ep2,2.0)

    else:
        # first indirect band gap terms
        energy_mask = energies >= Eg1T - Ep1
        dsigmadc1[energy_mask] += C1*dA1*np.power(energies[energy_mask] - Eg1T + Ep1,2.0)/(np.exp(Ep1/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg1T + Ep1
        dsigmadc1[energy_mask] += C1*dA1*np.power(energies[energy_mask] - Eg1T - Ep1,2.0)/(1 - np.exp(-Ep1/(k*temperature)))
        
        energy_mask = energies >= Eg1T - Ep2
        dsigmadc1[energy_mask] += C2*dA1*np.power(energies[energy_mask] - Eg1T + Ep2,2.0)/(np.exp(Ep2/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg1T + Ep2
        dsigmadc1[energy_mask] += C2*dA1*np.power(energies[energy_mask] - Eg1T - Ep2,2.0)/(1 - np.exp(-Ep2/(k*temperature)))
    
    # convert to units of cm^2/g
    dsigmadc1 *= 1/rho_Si
    
    return dsigmadc1

def compute_dsigmadA2(energies,temperature,constants):
    """
    Function to compute the dsigma/dA2 for the indirect absorption model.
    """
    
    rho_Si, c, h, k, gamma, beta, Eg20, Egd0, Ad, C1, C2, Ep1, Ep2, c0, c1, A2, Eg10 = unbox_indirectabsorption_Si_params(constants)
    
    dsigmadA2 = np.zeros(shape=len(energies))
    
    
    # determine the band gap energies from the given temperature
    Eg2T = Eg20 - ((beta*temperature*temperature)/(temperature + gamma))
    
    if temperature == 0:
        # second indirect band gap terms
        energy_mask = energies >= Eg2T - Ep1
        dsigmadA2[energy_mask] += 0
        
        energy_mask = energies >= Eg2T + Ep1
        dsigmadA2[energy_mask] += C1*np.power(energies[energy_mask] - Eg2T - Ep1,2.0)
        
        energy_mask = energies >= Eg2T - Ep2
        dsigmadA2[energy_mask] += 0
        
        energy_mask = energies >= Eg2T + Ep2
        dsigmadA2[energy_mask] += C2*np.power(energies[energy_mask] - Eg2T - Ep2,2.0)

    else:
        # second indirect band gap terms
        energy_mask = energies >= Eg2T - Ep1
        dsigmadA2[energy_mask] += C1*np.power(energies[energy_mask] - Eg2T + Ep1,2.0)/(np.exp(Ep1/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg2T + Ep1
        dsigmadA2[energy_mask] += C1*np.power(energies[energy_mask] - Eg2T - Ep1,2.0)/(1 - np.exp(-Ep1/(k*temperature)))
        
        energy_mask = energies >= Eg2T - Ep2
        dsigmadA2[energy_mask] += C2*np.power(energies[energy_mask] - Eg2T + Ep2,2.0)/(np.exp(Ep2/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg2T + Ep2
        dsigmadA2[energy_mask] += C2*np.power(energies[energy_mask] - Eg2T - Ep2,2.0)/(1 - np.exp(-Ep2/(k*temperature)))
    
    # convert to units of cm^2/g
    dsigmadA2 *= 1/rho_Si
    
    return dsigmadA2

def compute_dsigmadEg10(energies,temperature,constants):
    """
    Function to compute the dsigma/dEg10 for the indirect absorption model
    """
    
    rho_Si, c, h, k, gamma, beta, Eg20, Egd0, Ad, C1, C2, Ep1, Ep2, c0, c1, A2, Eg10 = unbox_indirectabsorption_Si_params(constants)
    
    dsigmadEg10 = np.zeros(shape=len(energies))
    
    # determine A1 proportionality constant
    A1 = c0*np.exp(-c1*temperature)
    
    # determine the band gap energies from the given temperature
    Eg1T = Eg10 - ((beta*temperature*temperature)/(temperature + gamma))
    
    if temperature == 0:
        # first indirect band gap terms
        energy_mask = energies >= Eg1T - Ep1
        dsigmadEg10[energy_mask] += 0
        
        energy_mask = energies >= Eg1T + Ep1
        dsigmadEg10[energy_mask] += C1*A1*(energies[energy_mask] - Eg1T - Ep1)*(-2)
        
        energy_mask = energies >= Eg1T - Ep2
        dsigmadEg10[energy_mask] += 0
        
        energy_mask = energies >= Eg1T + Ep2
        dsigmadEg10[energy_mask] += C2*A1*(energies[energy_mask] - Eg1T - Ep2)*(-2)

    else:
        # first indirect band gap terms
        energy_mask = energies >= Eg1T - Ep1
        dsigmadEg10[energy_mask] += C1*A1*(-2)*(energies[energy_mask] - Eg1T + Ep1)/(np.exp(Ep1/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg1T + Ep1
        dsigmadEg10[energy_mask] += C1*A1*(-2)*(energies[energy_mask] - Eg1T - Ep1)/(1 - np.exp(-Ep1/(k*temperature)))
        
        energy_mask = energies >= Eg1T - Ep2
        dsigmadEg10[energy_mask] += C2*A1*(-2)*(energies[energy_mask] - Eg1T + Ep2)/(np.exp(Ep2/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg1T + Ep2
        dsigmadEg10[energy_mask] += C2*A1*(-2)*(energies[energy_mask] - Eg1T - Ep2)/(1 - np.exp(-Ep2/(k*temperature)))
    
    # convert to units of cm^2/g
    dsigmadEg10 *= 1/rho_Si
    
    return dsigmadEg10

def compute_indirect_absorption_uncertainty(energies,temperature,constants):
    """
    Compute the standard deviation of the indirect absorption model in Si.
    """
    covar_matrix = constants.Si_params.indirect_absorption_model_params.c0_c1_A2_Eg10_covar.value
    
    dsigmadc0 = compute_dsigmadc0(energies,temperature,constants)
    dsigmadc1 = compute_dsigmadc1(energies,temperature,constants)
    dsigmadA2 = compute_dsigmadA2(energies,temperature,constants)
    dsigmadEg10 = compute_dsigmadEg10(energies,temperature,constants)
    
    # diagonal terms
    indirect_abs_unc = covar_matrix[0,0]*np.power(dsigmadc0,2.0) + covar_matrix[1,1]*np.power(dsigmadc1,2.0) + covar_matrix[2,2]*np.power(dsigmadA2,2.0) + covar_matrix[3,3]*np.power(dsigmadEg10,2.0)
    
    # off-diagonal terms
    indirect_abs_unc += 2*covar_matrix[0,1]*dsigmadc0*dsigmadc1 + 2*covar_matrix[0,2]*dsigmadc0*dsigmadA2 + 2*covar_matrix[0,3]*dsigmadc0*dsigmadEg10 + 2*covar_matrix[1,2]*dsigmadc1*dsigmadA2 + 2*covar_matrix[1,3]*dsigmadc1*dsigmadEg10 + 2*covar_matrix[2,3]*dsigmadA2*dsigmadEg10
    
    return np.sqrt(indirect_abs_unc)
    
    

def compute_indirect_absorption(energies,temperature,constants):
    """
    Function that will compute the values of the photoelectric absorption 
    cross section using the fitted indirect absorption model.
    
    Parameters
    ----------
    energies : 1d numpy array
        Array of absorption of which to obtain values of the photoelectric 
        absorption cross section (in units of keV). For DM absorption models, 
        the aborption energy is often equal to the DM mass.
    temperature : float
        Operating temperature of the experiment. Necessary for evaluating the 
        temperature-dependent region of the photoelectric absorption cross 
        section at energies below ~4 eV (in units of Kelvin).
    constants : python class object
        python class object containing model parameters and constants.
    
    Return
    ------
    indirect_abs : 1d numpy array
        Array of the computed photoelectric absorption cross section values. 
        The array will have the same length as energies.
    
    """
    
    rho_Si, c, h, k, gamma, beta, Eg20, Egd0, Ad, C1, C2, Ep1, Ep2, c0, c1, A2, Eg10 = unbox_indirectabsorption_Si_params(constants)
    
    indirect_abs = np.zeros(shape=len(energies))

    # determine A1 proportionality constant
    A1 = c0*np.exp(-c1*temperature)
    
    # determine the band gap energies from the given temperature
    Eg1T = Eg10 - ((beta*temperature*temperature)/(temperature + gamma))
    Eg2T = Eg20 - ((beta*temperature*temperature)/(temperature + gamma))
    EgdT = Egd0 - ((beta*temperature*temperature)/(temperature + gamma))
    
    if temperature == 0:
        # first indirect band gap terms
        energy_mask = energies >= Eg1T - Ep1
        indirect_abs[energy_mask] += 0
        
        energy_mask = energies >= Eg1T + Ep1
        indirect_abs[energy_mask] += C1*A1*np.power(energies[energy_mask] - Eg1T - Ep1,2.0)
        
        energy_mask = energies >= Eg1T - Ep2
        indirect_abs[energy_mask] += 0
        
        energy_mask = energies >= Eg1T + Ep2
        indirect_abs[energy_mask] += C2*A1*np.power(energies[energy_mask] - Eg1T - Ep2,2.0)
        
        # second indirect band gap terms
        energy_mask = energies >= Eg2T - Ep1
        indirect_abs[energy_mask] += 0
        
        energy_mask = energies >= Eg2T + Ep1
        indirect_abs[energy_mask] += C1*A2*np.power(energies[energy_mask] - Eg2T - Ep1,2.0)
        
        energy_mask = energies >= Eg2T - Ep2
        indirect_abs[energy_mask] += 0
        
        energy_mask = energies >= Eg2T + Ep2
        indirect_abs[energy_mask] += C2*A2*np.power(energies[energy_mask] - Eg2T - Ep2,2.0)
    else:
        # first indirect band gap terms
        energy_mask = energies >= Eg1T - Ep1
        indirect_abs[energy_mask] += C1*A1*np.power(energies[energy_mask] - Eg1T + Ep1,2.0)/(np.exp(Ep1/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg1T + Ep1
        indirect_abs[energy_mask] += C1*A1*np.power(energies[energy_mask] - Eg1T - Ep1,2.0)/(1 - np.exp(-Ep1/(k*temperature)))
        
        energy_mask = energies >= Eg1T - Ep2
        indirect_abs[energy_mask] += C2*A1*np.power(energies[energy_mask] - Eg1T + Ep2,2.0)/(np.exp(Ep2/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg1T + Ep2
        indirect_abs[energy_mask] += C2*A1*np.power(energies[energy_mask] - Eg1T - Ep2,2.0)/(1 - np.exp(-Ep2/(k*temperature)))
        
        # second indirect band gap terms
        energy_mask = energies >= Eg2T - Ep1
        indirect_abs[energy_mask] += C1*A2*np.power(energies[energy_mask] - Eg2T + Ep1,2.0)/(np.exp(Ep1/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg2T + Ep1
        indirect_abs[energy_mask] += C1*A2*np.power(energies[energy_mask] - Eg2T - Ep1,2.0)/(1 - np.exp(-Ep1/(k*temperature)))
        
        energy_mask = energies >= Eg2T - Ep2
        indirect_abs[energy_mask] += C2*A2*np.power(energies[energy_mask] - Eg2T + Ep2,2.0)/(np.exp(Ep2/(k*temperature)) - 1)
        
        energy_mask = energies >= Eg2T + Ep2
        indirect_abs[energy_mask] += C2*A2*np.power(energies[energy_mask] - Eg2T - Ep2,2.0)/(1 - np.exp(-Ep2/(k*temperature)))
        
    # direct bandgap term
    energy_mask = energies >= EgdT
    indirect_abs[energy_mask] += Ad*np.power(energies[energy_mask] - EgdT,0.5)
    
    # convert to units of cm^2/g
    indirect_abs *= 1/rho_Si
    
    return indirect_abs