import numpy as np
import warnings
from scipy.interpolate import interp1d
import xml.etree.ElementTree as ET
import pkg_resources
from os import path

import physicsvalues as pvl
from .indirect_absorption_model_Si import *

__all__ = [
    "load_data",
    "grab_pexsec_Si",
    "grab_conductivity_imaginary",
]



Sidata_str = pkg_resources.resource_filename(__name__, 'datafiles/optical_data_Si.xml')
Gedata_str = pkg_resources.resource_filename(__name__, 'datafiles/optical_data_Ge.xml')

mytree_Si = ET.parse(Sidata_str)
myroot_Si = mytree_Si.getroot()

mytree_Ge = ET.parse(Gedata_str)
myroot_Ge = mytree_Ge.getroot()

# runtime warning messages can pop up in the indirect absorption model - we will ignore these
warnings.filterwarnings("ignore",category=RuntimeWarning)


def load_data(target,data_type,physicsvalues_obj=None,source=None,interp_type=None):
    """
    Function to load optical data for various targets. The data is loaded 
    from xml files. The function will also read the x and y units from the
    xml file, and convert the data to the required units.
    
    Parameters
    ----------
    target : string
        Target material to obtain optical data for. Currently optical data 
        exists for Si and Ge.
    data_type : string
        The type of optical data to obtain. The options are 'pexsec_data', 
        'index_of_refraction_data', or 'imaginary_part_complex_conductivity'. 
        Not all types of optical data are available for all target materials.
    physicsvalues_obj : Dictionary (optional)
        an object of the Dictionary class with the constants. Can be obtained 
        from physicsvalues.load_constants() If None, the constants from 
        physicsvalues.default_constants are used.
    source : string (optional)
        For certain types of optical data, data is available for multiple sources. 
        Currently this only applies for the 'pexsec_data' for Si, in which case 
        the options are 'Henke', 'XCOM', and 'HOC'. Default is None.
    interp_type : string (optional)
        Type of interpolation if required. Options are 'linear' and 
        'inversecubic'. Default is None.
        
    Return
    ------
    data_output : ndarray or interpolation object
        If interp_type is None, then data_output is a 2D array containing the 
        loaded data. If interp_type is 'linear' or 'inversecubic', data_output 
        will be a 1D interpolation object.
    
    """
    
    # decide which set of constants to use
    if physicsvalues_obj is None:
        # use pre-loaded constants
        physicsvalues_obj = pvl.default_constants
    elif not isinstance(physicsvalues_obj,pvl.Dictionary) and not isinstance(physicsvalues_obj,pvl.ImmutableDictionary):
        raise TypeError('ERROR: physicsvalues_obj was passed but it is not of type physicsvalues Dictionary class.')
        
    c = physicsvalues_obj.physics_params.speed_of_light.value # speed of light (cm/s)
    h = physicsvalues_obj.physics_params.planck.value # Planck constant (eVs)
    
    if not isinstance(target,str):
        raise TypeError('ERROR: target argument must be a string.')
    elif target.lower() == 'si':
        rho = physicsvalues_obj.Si_params.Si_density.value*1e-3
        if not isinstance(data_type,str):
            raise TypeError('ERROR: data_type argument must be a string.')
        elif data_type == 'pexsec_data':
            if source is None:
                raise ValueError('ERROR: %s data for %s requires a source!'%(data_type,target))
            elif not isinstance(source,str):
                raise TypeError('ERROR: source argument is passed, but must be a string.')
            else:
                if source == 'Henke' or source == 'HOC' or source == 'XCOM':
                    data_element = mytree_Si.find(data_type).find(source)
                else:
                    raise NotImplementedError('ERROR: %s is not a recognized source of %s data for %s!'%(source,data_type,target))
            
        elif data_type == 'index_of_refraction_data':
            data_element = mytree_Si.find(data_type)
            
        elif data_type == 'imaginary_part_complex_conductivity':
            data_element = mytree_Si.find(data_type)
            
        else:
            raise NotImplementedError('ERROR: %s data is not available for %s target!'%(data_type,target))
            
    elif target.lower() == 'ge':
        if not isinstance(data_type,str):
            raise TypeError('ERROR: data_type argument must be a string.')
        elif data_type == 'pexsec_data':
            raise ValueError('ERROR: %s data is not yet available for %s target!'%(data_type,target))
            
        elif data_type == 'index_of_refraction_data':
            raise ValueError('ERROR: %s data is not yet available for %s target!'%(data_type,target))
            
        elif data_type == 'imaginary_part_complex_conductivity':
            data_element = mytree_Ge.find(data_type)
            
        else:
            raise NotImplementedError('ERROR: %s data is not available for %s target!'%(data_type,target))
    else:
        raise NotImplementedError('ERROR: data is not available for %s target! target '%target+\
                                  'argument must be one of the following options: "Si", "Ge".')
    
    
    # get the x and y units:
    xunits = data_element.find('x_units').text
    yunits = data_element.find('y_units').text
    
    # load the data
    data = np.array([[float(j) for j in i.split(', ')] for i in data_element.find('data').text.splitlines()])
    
    # convert the x values to keV
    if xunits == 'keV':
        pass
    elif xunits == 'eV':
        data[:,0] *= 1e-3
    elif xunits == 'MeV':
        data[:,0] *= 1e3
    elif xunits == 'GeV':
        data[:,0] *= 1e6
    elif xunits == 'um':
        # convert photon wavelength (in um) to energy (in keV)
        data[:,0] = 1e-3*(c*h)/(1e-4*data[:,0])
    else:
        raise ValueError('ERROR: x units of %s are not recognized and no conversion is available!'%xunits)
    
    # convert the y values to the appropriate units
    if data_type == 'pexsec_data':
        if yunits == 'cm^2/g':
            pass
        elif yunits == 'k':
            # k = alpha*c/(4*pi*f) = alpha*c*h/(4*pi*E) --> sigma_pe = alpha/rho = k*4*pi*E/(c*h*rho)
            # this conversion expects the x values to be in keV
            data[:,1] *= (4*np.pi*data[:,0]*1e3)/(c*h*rho)
        else:
            raise ValueError('ERROR: y units of %s are not recognized for %s and no conversion is available!'%(yunits,data_type))
    elif data_type == 'index_of_refraction_data':
        if yunits is not None:
            raise ValueError('ERROR: y units of %s are not recognized for %s and no conversion is available!'%(yunits,data_type))
    elif data_type == 'imaginary_part_complex_conductivity':
        if yunits != "eV":
            raise ValueError('ERROR: y units of %s are not recognized for %s and no conversion is available!'%(yunits,data_type))
        
    # interpolate the data, if necessary
    if interp_type is None:
        data_output = data
    elif not isinstance(interp_type,str):
        raise TypeError('ERROR: interp_type argument passed, but must be a string')
    else:
        if interp_type == 'linear':
            data_output = interp1d(data[:,0],data[:,1])
        elif interp_type == 'inversecubic':
            data_output = interp1d(data[:,0],np.power(data[:,1],-(1/3)))
        else:
            raise ValueError('ERROR: interpolation type %s not recognized!'%interp_type)
    return data_output



def grab_pexsec_Si(energies,temperature,values='nominal',param='pe',physicsvalues_obj=None,savepath=None):
    """"
    Function to grab the values of the photoelectric absorption cross 
    section in Si for the specified absorption energies and temperature.
    
    For absorption energies below ~3.2 eV, the photoelectric absorption 
    cross section values are determine from the fitted indirect absorption model. 
    More details can be found in https://doi.org/10.1063/5.0038392.
    
    For absorption enegies above 3.2 eV and below 1 keV, the photoelectric 
    absorption cross section values are taken from "Silicon (Si)" in the 
    Handbook of Optical Constants by D. Edwards 
    (https://doi.org/10.1016/B978-012544415-6.50027-3).
    
    For absorption enegies above 1 keV and below 20 keV, the photoelectric 
    absorption cross section values are taken from "X-Ray Interactions: 
    Photoabsorption, Scattering, Transmission, and Reflection at E = 50-30,000 eV, 
    Z = 1-92" by Henke et. al. (https://doi.org/10.1006/adnd.1993.1013).
    
    For absorption energies above 20 keV, the photoelectric absorption 
    cross section values are taken from the NIST XCOM database (version 1.5) 
    by Berger et. al. (https://dx.doi.org/10.18434/T48G6X).
    
    
    Parameters
    ----------
    energies : 1d numpy array
        array of absorption of which to obtain values of the photoelectric 
        absorption cross section (in units of keV). For DM absorption models,
        the aborption energy is often equal to the DM mass.
        
        This tool will return photoelectric absorption cross section values 
        for absorption energies up to 100 GeV. The photoelectric absorption 
        cross section values for energies below the first indirect band gap 
        of Si will have a value of zero.
    temperature : float
        temperature of the absorbing material. Necessary for evaluating the 
        temperature-dependent region of the photoelectric absorption cross 
        section at energies below ~4 eV (in units of Kelvin).
    
    Optional Parameters
    -------------------
    values : string, default = 'nominal'
        for the fitted model describing the temperature-dependent region of 
        the photoelectric absorption cross section at energies below ~4 eV, 
        whether the nominal, upper, or lower values are returned. Options 
        are 'nominal', 'lower', or 'upper'.
    param : string, default = 'pe'
        the parameter that is returned. Options are 'pe', the photoelectric 
        absorption cross section, in units of cm^2/g; or 'sig1', the real part 
        of the complex conductivity, in units of eV. 
    physicsvalues_obj : Dictionary, default = None
        an object of the Dictionary class with the constants. Can be obtained 
        from physicsvalues.load_constants() If None, the constants from 
        physicsvalues.default_constants are used.
    savepath : string
        if this parameter is not None, the data will be save in a text file 
        at the location provided by this argument. In this case, the data 
        is not returned by the function.
    
    Return
    ------
    pexsec : 1d numpy array
        array of the photoelectric absorption cross section value evaluated at each 
        energy in the energies input argument. The array will have the same length 
        as energies. This is returned only if savepath is None.
    
    """
    
    # check to make sure arguments are okay
    if not isinstance(temperature,int) and not isinstance(temperature,float):
        raise TypeError('ERROR: temperature argument must be either of int or float type.')
    elif temperature < 0:
         raise ValueError('ERROR: temperature value of %s falls below zero. Make sure '%temperature+\
                          'the temperature is given in units of Kelvin!')
    if not isinstance(values,str):
        raise TypeError('ERROR: values aregument must be a string.')
    elif values.lower() != 'nominal' and values.lower() != 'upper' and values.lower() != 'lower':
        raise ValueError("ERROR: the values argument '%s' is not accepted. Accepted "%values+\
                         "options include 'nominal', 'upper', or 'lower'!")
    if not isinstance(param,str):
        raise TypeError('ERROR: param argument must be a string.')
    elif param.lower() != 'pe' and param.lower() != 'sig1':
        raise ValueError("ERROR: the param argument '%s' is not accepted. Accepted "%param+\
                         "options include 'pe' or 'sig1'!")
    if isinstance(energies,list):
        energies = np.asarray(energies)
    elif isinstance(energies,int) or isinstance(energies,float):
        energies = np.asarray([energies])
    if isinstance(energies,np.ndarray):
        if energies.ndim != 1:
            raise ValueError('ERROR: energies argument must be an array with 1 dimension.')
        if max(energies) > 1e8:
            raise ValueError('ERROR: energy input values exceed 100 GeV for this tool. '+\
                             'Make sure input energies are in units of keV!')
        if any(energies<0):
            raise ValueError('ERROR: energies argument must contain only postive numbers.')
    else:
        raise TypeError('ERROR: energies argument is not or cannot be converted to a numpy 1D array.')
    if savepath is not None:
        if not isinstance(savepath,str):
            raise TypeError('ERROR: savepath argument must be a string')
        elif not path.exists(savepath):
            raise ValueError("ERROR: the directory path %s does not exist!"%savepath)
    
    # decide which set of constants to use
    if physicsvalues_obj is None:
        # use pre-loaded constants
        physicsvalues_obj = pvl.default_constants
    elif not isinstance(physicsvalues_obj,pvl.Dictionary) and not isinstance(physicsvalues_obj,pvl.ImmutableDictionary):
        raise TypeError('ERROR: physicsvalues_obj was passed but it is not of type physicsvalues Dictionary class.')
    
    # load the necesary datafiles
    # these are interpolation object converted to the correct units
    optical_constants_linear = load_data('Si','pexsec_data',physicsvalues_obj,source='HOC',interp_type='linear')
    optical_constants_cubic = load_data('Si','pexsec_data',physicsvalues_obj,source='HOC',interp_type='inversecubic')
    henke = load_data('Si','pexsec_data',physicsvalues_obj,source='Henke',interp_type='inversecubic')
    xcom = load_data('Si','pexsec_data',physicsvalues_obj,source='XCOM',interp_type='inversecubic')
    
    # initialize array for cross section values
    pexsec = np.zeros(shape=len(energies))
    
    # the transition point between the indirect absorption model and the data 
    # from the Handbook of Optical Constants is variable.
    # the transition point is determined from where the fractional difference 
    # between the two sources of data is the smallest.
    
    # setup a separate energy grid
    transition_egrid = np.linspace(3.2e-3,4e-3,100)
    d1 = optical_constants_linear(transition_egrid)
    d2 = compute_indirect_absorption(transition_egrid*1e3,temperature,physicsvalues_obj)
    diff = np.abs(d1-d2)/d1
    transition_point = transition_egrid[np.argmin(diff)]
    
    
    # for energies below the transition point determined above, use the fitted indirect absorption model
    Emask = energies < transition_point
    if len(energies[Emask]) > 0:
        pexsec[Emask] = compute_indirect_absorption(energies[Emask]*1e3,temperature,physicsvalues_obj)
        if values == 'upper':
            pexsec[Emask] += compute_indirect_absorption_uncertainty(energies[Emask]*1e3,temperature,physicsvalues_obj)
        elif values == 'lower':
            pexsec[Emask] += -1*compute_indirect_absorption_uncertainty(energies[Emask]*1e3,temperature,physicsvalues_obj)
    
    # for energies above 3.25 eV and below 1 keV, use the data from the Hanbook of Optical Constants
    # for energies below 200 eV, linear interpolation is sufficient
    Emask = (energies >= transition_point)&(energies < 0.2)
    if len(energies[Emask]) > 0:
        pexsec[Emask] = optical_constants_linear(energies[Emask])
    
    # for energies above 200 eV, inverse cubic interpolation more appropriate
    Emask = (energies >= 0.2)&(energies < 1)
    if len(energies[Emask]) > 0:
        pexsec[Emask] = np.power(optical_constants_cubic(energies[Emask]),-3.0)
    
    # for energies above 1 keV and below 20 keV, use the Henke et. al. data
    Emask = (energies >= 1)&(energies < 20)
    if len(energies[Emask]) > 0:
        pexsec[Emask] = np.power(henke(energies[Emask]),-3.0)
    
    # for energies above 20 keV, use the XCOM data
    Emask = (energies >= 20)
    if len(energies[Emask]) > 0:
        pexsec[Emask] = np.power(xcom(energies[Emask]),-3.0)
    
    # if the sigma1 parameter is to be retunred, do the conversion
    if param.lower() == 'sig1':
        rho_Si = physicsvalues_obj.Si_params.Si_density.value*1e-3 # density of Si (g/cm^3)
        c = physicsvalues_obj.physics_params.speed_of_light.value # speed of light (cm/s)
        h = physicsvalues_obj.physics_params.planck.value # Planck constant (eVs)
        
        n_data = load_data('Si','index_of_refraction_data',physicsvalues_obj)
        # this will also extrapolate the data. For energies above 1 keV, n --> 1.
        n_interp = interp1d(n_data[:,0],n_data[:,1],bounds_error=False,fill_value=(n_data[-1,1],1))
        pexsec *= rho_Si*c*h*n_interp(energies)/(2*np.pi)
    
    # if a savepath is provided, save the data to a text file
    # if not, return the data
    if savepath is not None:
        if param.lower()=='pe':
            filename = 'pexsec_Si_keV_cm2g.txt'
        elif param == 'sig1':
            filename = 'conductivity_Si_keV_eV_real.txt'
        np.savetxt(savepath+filename,np.column_stack((energies,pexsec)),fmt='%.4e',delimiter='\t')
        return
    else:
        return pexsec
    
def grab_conductivity_imaginary(energies,target,physicsvalues_obj=None):
    """"
    Function to grab the values of the imaginary part of the complex conductivity 
    for the specified absorption energies. Currently data is only available for 
    silicon and germanium.
    
    Parameters
    ----------
    energies : 1d numpy array
        array of absorption of which to obtain values of the imaginary part of the 
        complex conductivity (in units of keV). For DM absorption models, the aborption 
        energy is often equal to the DM mass. This tool will return values for absorption 
        energies between 1.1 eV and 1.988 keV (Si) and between 0.645 eV and 0.516 keV (Ge).
    target : string
        load the data file for a specific target material.
    
    Optional Parameters
    -------------------
    physicsvalues_obj : Dictionary
        an object of the Dictionary class with the constants. Can be obtained from 
        physicsvalues.load_constants() If None, the constants from 
        physicsvalues.default_constants are used.
    
    Return
    ------
    sig2 : 1d numpy array
        array of imaginary part of the complex conductivity (sigma 2) values. 
        The array will have the same length as energies.
    
    """
    # check input arguments
    if not isinstance(target,str):
        raise TypeError('ERROR: target argument must be a string.')
    elif target.lower() != 'si' and target.lower() != 'ge':
        raise ValueError('ERROR: target argument must be one of the followinng options: "Si", "Ge".')
    
    if isinstance(energies,list):
        energies = np.asarray(energies)
    elif isinstance(energies,int) or isinstance(energies,float):
        energies = np.asarray([energies])
    if isinstance(energies,np.ndarray):
        if energies.ndim != 1:
            raise ValueError('ERROR: energies argument must be an array with 1 dimension.')
        if target.lower() == 'si':
            if any(energies<0.001094) or any(energies>1.988):
                raise ValueError('ERROR: energies argument must contain only values between 0.001094 and 1.988 keV.')
        elif target.lower() == 'ge':
            if any(energies<0.000645123) or any(energies>0.516076):
                raise ValueError('ERROR: energies argument must contain only values between 0.000645123 and 0.516 keV.')
    else:
        raise TypeError('ERROR: energies argument is not or cannot be converted to a numpy 1D array.')
    
    # decide which set of constants to use
    if physicsvalues_obj is None:
        # use pre-loaded constants
        physicsvalues_obj = pvl.default_constants
    elif not isinstance(physicsvalues_obj,pvl.Dictionary) and not isinstance(physicsvalues_obj,pvl.ImmutableDictionary):
        raise TypeError('ERROR: physicsvalues_obj was passed but it is not of type physicsvalues Dictionary class.')
    
    # load the datafile
    sig2_interp = load_data(target,'imaginary_part_complex_conductivity',physicsvalues_obj,interp_type='linear')
    
    sig2 = sig2_interp(energies)
    
    return sig2