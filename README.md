# DM Modeling Optical Constants Package
Package contains optical data for various materials, as well as tools for loading and computing optical data that is used for dark matter modeling.

This package is made available by the SuperCDMS Collaboration to provide in the public domain the software used for SuperCDMS studies. Though validated by the Collaboration, there is no warranty or guarantee that it is correct. Users should perform their own validation of the functions contained within this package.

Contact: Matt Wilson (matthew.wilson@kit.edu)

---

## Table of Contents
  1. [Introduction](#introduction)
  2. [Installation](#install)
      1. [Install Dependencies](#install_dep)
  3. [Photoelectric Absorption](#photoelectric-absorption)
      1. [Silicon](#si_pe)
      2. [Germanium](#ge_pe)
  4. [Complex Conductivity](#complex-conductivity)
      1. [Real Part of the Complex Conductivity](#sigma1)
          1. [Silicon](#si_sigma1)
          2. [Germanium](#ge_sigma1)
      2. [Imaginary Part of the Complex Conductivity](#sigma2)
          1. [Silicon and Germanium](#si_ge_sigma2)
  5. [Project Status](#status)
  6. [Issue Tracking](#issue_tracking)

---

## Introduction <a name="introduction"></a>
This repository contains relevant tools and resources relating to optical constants for certain target materials. These constants are often used for certain dark matter models, including dark photon absorption and axion-like particle absorption. This repository currently contains data/tools for optical constants for the following target materials:
- Silicon (Si)

Furthermore, this repository contains data/tools related to the following optical constants:
- Photoelectric absorption cross section ($\sigma_{pe}$)
- Complex optical conductivity ($\sigma_{1} + i \sigma_{2}$)

All of the optical constants data are found in the `datafiles/` directory for the corresponding target materials. The tools for extracting the data found in the `optical_constants_tools.py` file. The following sections will describe the various optical constants data available in this repository, as well as how to use the tools to extract the data.

**Note:** throughout this README and package, the photoelectric cross section $\sigma_{pe}$ is obtained in units of cm$^2$/g. This is also referred to as a mass absorption or attenuation coefficient, or a cross section per gram of material. It is referred to as a cross section here due to precedent set in literature. 

---

## Installation <a name="install"></a>
#### Install Dependencies: <a name="install_dep"></a>
-  numpy
-  scipy
-  [dmmodeling_physicsvalues >= 2.2.0](https://gitlab.com/supercdms/public/dmmodeling_physicsvalues)

This git repository is also an installable python package. There are various ways to install a python package, but a common way is to first git clone the repository into your local directory. Once that is done, the package can be installed as follows:
```
pip install [path-to-dmmodeling_opticalconstants-package]/dmmodeling_opticalconstants
```
or
```
pip install --upgrade [path-to-dmmodeling_opticalconstants-package]/dmmodeling_opticalconstants
```
Depending on the user's setup, the installation may or may not be done within a python venv. In some cases, it may be necessary to add the `--user` argument to the pip install command. If the package is installed from a Juptyer notebook, restart the kernal after the package is successfully installed before importing.

This package has an installation dependencies on another custom python package(s) ([dmmodeling_physicsvalues](https://gitlab.com/supercdms/public/dmmodeling_physicsvalues)). The installation will attempt to find if the package(s) exists in the correct version(s) and if not, attempt to retrieve the package(s) from Gitlab and install them. This installation process requires the git executable to be available on the system path. ***In some cases, the installation fails to access the dependent package(s) from GitLab and fails to install them. If this happens, the user should manually install the dependent package(s) with the correct version.***

***Package installation works with pip version 21.3.1. Try updating pip if the installation fails.***

---

## Photoelectric Absorption <a name="photoelectric-absorption"></a>
The photoelectric absorption cross section ($\sigma_{pe}$) data are derived from an amalgamation of various sources, even for the same target material. The tool will return the $\sigma_{pe}$ data corresponding to specific absorption energies. Note that for certain dark matter models, such as dark photon absorption and axion-like particle absorption, the absorption energy is equivalent to the mass of the dark matter particle. 

**Note:** throughout this README and package, the photoelectric cross section $\sigma_{pe}$ is obtained in units of cm$^2$/g. This is also referred to as a mass absorption or attenuation coefficient, or a cross section per gram of material. It is referred to as a cross section here due to precedent set in literature. 

### Silicon <a name="si_pe"></a>
The $\sigma_{pe}$ data for Si is available for absorption energies down to the lowest indirect band gap of Si ($\sim 1.13$eV) and up to 100 GeV. The data are derived/computed from four different sources, each spanning a different absorption energy range. The sources, and their respective absorption energy range, are as follows:
1. For absorption energies $\lesssim$ 3.2 eV, the $\sigma_{pe}$ data are computed using a fitted model of the indirect absorption in Si at low energies and temperatures. This model is used to describe the temperature dependence of indirect absorption the occurs at low absorption energies. The model uses the fitted values published in [Photoelectric absorption cross section of silicon near the bandgap from room temperature to sub-Kelvin temperature](https://doi.org/10.1063/5.0038392) by Stanford *et. al*. ***Starting from version 1.2.0, The indirect absorption model for Si is using updated values of the fit parameters. The updated values are used in order to provide a better propagation of uncertainty in this model. Source for these updated parameters to come.***
2. For absorption energies $\gtrsim$ 3.2 eV and < 1 keV, the $\sigma_{pe}$ data are extracted from the article [Silicon (Si)](https://doi.org/10.1016/B978-012544415-6.50027-3) by D. Edwards found in the Handbook of Optical Constants.
3. For absorption energies $\geq$ 1 keV and < 20 keV, the $\sigma_{pe}$ data are extracted from [X-Ray Interactions: Photoabsorption, Scattering, Transmission, and Reflection at E = 50-30,000 eV, Z = 1-92](https://doi.org/10.1006/adnd.1993.1013) by Henke *et. al*.
4. For absorption energies $\geq$ 20 keV, the $\sigma_{pe}$ data are extracted from the [NIST XCOM database (version 1.5)](https://dx.doi.org/10.18434/T48G6X) by Berger *et. al*.

These data are stored in the `opticalconstants/datafiles/optical_data_Si.xml` file within the "pexsec_data" element. The following plots show the full range of $\sigma_{pe}$ data available in this repository for Si. The second plot breaks down the $\sigma_{pe}$ data into the various sources where it is derived or extracted from.

![pexsec_Si_full](Images/pexsec_Si_full.png) ![pexsec_Si_full_split](Images/pexsec_Si_full_split.png) 

The $\sigma_{pe}$ data for Si can be extracted calling the `grab_pexsec_Si` function in `optical_constants_tools.py`. The `grab_pexsec_Si` function has the following arguments:
  -  energies : 1d numpy array 
      - array of absorption energies of which to extract the $\sigma_{1}$ values for. The absorption energies are expected to be in units of keV. The function will return an error of this argument exceeds 100 GeV. For absorption energies below the lowest indirect band gap of Si ($\sim$ 1.13 eV), the function will return $\sigma_{1}$ values of zero.
  - temperature : float
      - operating temperature of the absorbing material. The parameter is required for computing the indirect absorption model, which has a temperature dependence. This argument will not affect any of the $\sigma_{1}$ values $\gtrsim$ 3.2 eV. The temperature is expected to be in units of Kelvin.
  - values : string, default = 'nominal'
      - for the fitted model describing the temperature-dependent region of the photoelectric absorption cross section at energies $\lesssim$ 3 eV, whether the nominal, upper, or lower curve is returned. Options are 'nominal', 'lower', or 'upper'. See below for further details.
  - param : string, default = 'pe'
      - describing which parameter is returned. Options are 'pe', the photoelectric absorption cross section, in units of cm^2/g; or 'sig1', the real part of the complex conductivity, in units of eV.
  - physicsvalues_obj : Dictionary, default = None
      - an object of the Dictionary class with the constants. Can be obtained from physicsvalues.load_constants() If None, the constants from physicsvalues.default_constants are used.
  - savepath : string, default = None
      - if this parameter is not None, the data will be save in a text file at the location specified by this argument. When a valid savepath is passed, the function will have no return argument.

Below is an example of how the `grab_pexsec_Si` function can be used.

```python
import numpy as np
import opticalconstants as OPC

energies_keV = np.logspace(-3,3,100)

my_temps = [0.05,77,200,300] # temperatures in units of Kelvin

# grab the pexsec for different temperatures:
for t in my_temps:
    pe_values = OPC.grab_pexsec_Si(energies_keV,
                                                       t,
                                                       values='nominal',
                                                       param='pe')

```

The plot below shows the temperature dependence of the [indirect absorption model](https://doi.org/10.1063/5.0038392) in Si. Note that this argument does not affect any of the $\sigma_{pe}$ values $\gtrsim$ 3.2 eV.

![pexsec_Si_tempvar](Images/pexsec_Si_tempvar.png)

The plot below shows the difference between the 'nominal', 'upper', and 'lower' curves of the [indirect absorption model](https://doi.org/10.1063/5.0038392). The upper and lower curves are defined as the $\pm 1 \sigma$ uncertainty values over energy. The curves are found by standard error propagation using the covariance matrix of the fit parameters and the partial derivatives of the model with respect to the fit paraneters. ***Source for the updated error propagation still to come.***

![pexsec_Si_uncvals](Images/pexsec_Si_uncvals.png)

Note that this argument does not affect any of the $\sigma_{pe}$ values $\gtrsim$ 3.2 eV.

**Transition Between the Indirect Absorption Model and Data from the Handbook of Optical Constants**

The transition point in absorption energy between the $\sigma_{pe}$ values computed using the [indirect absorption model](https://doi.org/10.1063/5.0038392) and the $\sigma_{pe}$ values extracted from the [Handbook of Optical Constants](https://doi.org/10.1016/B978-012544415-6.50027-3) is varied based on the temperature. This is done to ensure a smooth transition between $\sigma_{pe}$ values obtained from the two sources. The transition point is defined as the absorption energy where the fractional difference between the $\sigma_{pe}$ values computed using the [indirect absorption model](https://doi.org/10.1063/5.0038392) and the $\sigma_{pe}$ values extracted from the [Handbook of Optical Constants](https://doi.org/10.1016/B978-012544415-6.50027-3) is the smallest, for absorption energies above 3.2 eV. The overall effect of this varying transition point is small; at 0 K, the transition point is found at 3.25 eV, and at 300 K, the transition point is found at 3.32 eV. Note that the [indirect absorption model](https://doi.org/10.1063/5.0038392) is valid for absorption energies up to 4 eV.

The plot below shows how the transition point between the two sources of $\sigma_{pe}$ values varies with temperature. 

![pexsec_Si_transition](Images/pexsec_Si_transition.png)

**Linear and Inverse-Cubic Interpolation**

The extracted $\sigma_{pe}$ data are interpolated in order to provide the $\sigma_{pe}$ values at the absorption energies specified by the user. In regions where the $\sigma_{pe}$ data looks like a straight line in log-log space, interpolated values are better described using an inverse-cubic interpolation method. Therefore for Si, the $\sigma_{pe}$ data are interpolated linearly for absorption energies below 200 eV, and are interpolated inverse-cubically for absorption energies above 200 eV.


### Germanium <a name="ge_pe"></a>
***The photoelectric absorption data/tools are not currently available for Ge!***

---

## Complex Conductivity <a name="complex-conductivity"></a>
The complex optical conductivity consists of two components: the real part ($\sigma_1$) and the imaginary part ($\sigma_2$). These parameters are required for the dark photon absorption model described in [Absorption of light dark matter in semiconductors](https://doi.org/10.1103/PhysRevD.95.023013) by Hochberg *et. al.* 

### Real Part of the Complex Conductivity <a name="sigma1"></a>
The real part of the complex conductivity ($\sigma_1$) is the product of the photoelectric absorption cross section ($\sigma_{pe}$) and the index of refraction ($n$), which some additional factors for unit conversions, for a given semiconductor target material. Specifically for $\sigma_{pe}$ in units of cm^2/g and $\sigma_{1}$ in units of eV, the two parameters are related by $\sigma_{1} = n \cdot \sigma_{pe} \cdot c \cdot \hbar \cdot \rho$, where $n$ is the index of refraction, $c$ is the speed of light, $\hbar$ is the reduced Planck's constant, and $\rho$ is the density of the target material. Because $\sigma_{1}$ and $\sigma_{pe}$ are proportional to each other, the tools for extracting $\sigma_{1}$ values for given absorption energies are the same [tools for extracting $\sigma_{pe}$ values](#photoelectric-absorption) with the necessary additional factors included.

#### Silicon <a name="si_sigma1"></a>
The $\sigma_{1}$ data for Si is available for absorption energies down to the lowest indirect band gap of Si ($\sim 1.13$eV) and up to 100 GeV. The data are derived/computed from four different sources, each spanning a different absorption energy range. The sources, and their respective absorption energy range, are as follows:
1. For absorption energies $\lesssim$ 3.2 eV, the $\sigma_{pe}$ data are computed using a fitted model of the indirect absorption in Si at low energies and temperatures. This model is used to describe the temperature dependence of indirect absorption the occurs at low absorption energies. The model uses the fitted values published in [Photoelectric absorption cross section of silicon near the bandgap from room temperature to sub-Kelvin temperature](https://doi.org/10.1063/5.0038392) by Stanford *et. al*. 
2. For absorption energies $\gtrsim$ 3.2 eV and $<$ 1 keV, the $\sigma_{pe}$ data are extracted from the article [Silicon (Si)](https://doi.org/10.1016/B978-012544415-6.50027-3) by D. Edwards found in the Handbook of Optical Constants.
3. For absorption energies $\geq$ 1 keV and $<$ 20 keV, the $\sigma_{pe}$ data are extracted from [X-Ray Interactions: Photoabsorption, Scattering, Transmission, and Reflection at E = 50-30,000 eV, Z = 1-92](https://doi.org/10.1006/adnd.1993.1013) by Henke *et. al*.
4. For absorption energies $\geq$ 20 keV, the $\sigma_{pe}$ data are extracted from the [NIST XCOM database (version 1.5)](https://dx.doi.org/10.18434/T48G6X) by Berger *et. al*.

These data are stored in the `opticalconstants/datafiles/optical_data_Si.xml` file within the "pexsec_data" element. Furthermore for converting the $\sigma_{pe}$ values to $\sigma_{1}$ values, the following data is used:
1. Index of refraction data extracted from the article [Silicon (Si)](https://doi.org/10.1016/B978-012544415-6.50027-3) by D. Edwards found in the Handbook of Optical Constants. The corresponding datafile is `opticalconstants/datafiles/optical_data_Si.xml` file within the "index_of_refraction_data" element. For absorption energies above 1 keV (up to where there is no more data from this source), the index of refraction data are extrapolated to a value of 1.

The following plots show the full range of $\sigma_{1}$ data available in this repository for Si. The second plot breaks down the $\sigma_{1}$ data into the various sources where it is derived or extracted from. Furthermore, the index of refraction data is also shown.

![sig1_Si_full](Images/sig1_Si_full.png) ![sig1_Si_full_split](Images/sig1_Si_full_split.png) ![n_Si](Images/n_Si.png) 

The $\sigma_{1}$ data for Si can be extracted calling the `grab_pexsec_Si` function in `optical_constants_tools.py`. The `grab_pexsec_Si` function has the following arguments:
  -  energies : 1d numpy array 
      - array of absorption energies of which to extract the $\sigma_{1}$ values for. The absorption energies are expected to be in units of keV. The function will return an error of this argument exceeds 100 GeV. For absorption energies below the lowest indirect band gap of Si ($\sim$ 1.13 eV), the function will return $\sigma_{1}$ values of zero.
  - temperature : float
      - operating temperature of the absorbing material. The parameter is required for computing the indirect absorption model, which has a temperature dependence. This argument will not affect any of the $\sigma_{1}$ values $\gtrsim$ 3.2 eV. The temperature is expected to be in units of Kelvin.
  - values : string, default = 'nominal'
      - for the fitted model describing the temperature-dependent region of the photoelectric absorption cross section at energies $\lesssim$ 3 eV, whether the nominal, upper, or lower curve is returned. Options are 'nominal', 'lower', or 'upper'.
  - param : string, default = 'pe'
      - describing which parameter is returned. Options are 'pe', the photoelectric absorption cross section, in units of cm^2/g; or 'sig1', the real part of the complex conductivity, in units of eV. For extracting $\sigma_{1}$ data, this argument must always be set to 'sig1'.
  - physicsvalues_obj : Dictionary, default = None
      - an object of the Dictionary class with the constants. Can be obtained from physicsvalues.load_constants() If None, the constants from physicsvalues.default_constants are used.
  - savepath : string, default = None
      - if this parameter is not None, the data will be save in a text file at the location specified by this argument. When a valid savepath is passed, the function will have no return argument.

Below is an example of how the `grab_pexsec_Si` function can be used.

```python
import numpy as np
import opticalconstants as OPC

energies_keV = np.logspace(-3,3,100)

my_temp = 1 # temperature in units of Kelvin

sig1_values = OPC.grab_pexsec_Si(energies_keV,
                                                   my_temp,
                                                   values='nominal',
                                                   param='sig1')

```

All effects related to the temperature dependence of the [indirect absorption model](https://doi.org/10.1063/5.0038392), the uncertainty in the fit parameters of the [indirect absorption model](https://doi.org/10.1063/5.0038392), the transition point between the data computed from the [indirect absorption model](https://doi.org/10.1063/5.0038392) and the [Handbook of Optical Constants](https://doi.org/10.1016/B978-012544415-6.50027-3), and the linear vs. inverse-cubic extrapolation are the same as described in the [photoelectric absorption section for Si](#si_pe).

#### Germanium <a name="ge_sigma1"></a>
***The real part of the complex conductivity data/tools are not currently available for Ge!***

---

### Imaginary Part of the Complex Conductivity <a name="sigma2"></a>

#### Silicon and Germanium <a name="si_ge_sigma2"></a>
The data for the imaginary part of the complex conductivity ($\sigma_2$) for Si and Ge are extracted from [Absorption of light dark matter in semiconductors](https://doi.org/10.1103/PhysRevD.95.023013) by Hochberg *et. al.* The corresponding datafiles for Si and Ge are `opticalconstants/datafiles/optical_data_Si.xml` and `opticalconstants/datafiles/optical_data_Ge.xml` files respectively within the "imaginary_part_complex_conductivity" element.

The plot below shows the absolute $\sigma_2$ values extraced for Si, with the blue and orange curves showing the negative and positive $\sigma_2$ values, respectively.

![sig2_Si](Images/sig2_Si.png)

The plot below shows the absolute $\sigma_2$ values extraced for Ge, with the blue and orange curves showing the negative and positive $\sigma_2$ values, respectively.

![sig2_Ge](Images/sig2_Ge.png)


The $\sigma_{2}$ data for Si and Ge can be extracted calling the `grab_conductivity_imaginary` function in `optical_constants_tools.py`. The `grab_conductivity_imaginary` function has the following arguments:
  - energies : 1d numpy array 
      - array of absorption energies of which to extract the $\sigma_{2}$ values for. The absorption energies are expected to be in units of keV.
  - target : string 
      - the target material of which to extract the $\sigma_{2}$ values for. Currently the options are 'Si' or 'Ge'.
  - physicsvalues_obj : Dictionary, default = None
      - an object of the Dictionary class with the constants. Can be obtained from physicsvalues.load_constants() If None, the constants from physicsvalues.default_constants are used.

Below is an example of how the `grab_conductivity_imaginary` function can be used. This function returns the data seen in the plots above.

```python
import numpy as np
import opticalconstants as OPC

energies_keV = np.logspace(-2,-1,100)

# extract values for Si
sig2_values_Si = OPC.grab_conductivity_imaginary(energies_keV,'Si')

# extract values for Ge
sig2_values_Ge = OPC.grab_conductivity_imaginary(energies_keV,'Ge')

```

---

## Project Status <a name="status"></a>
Potential or known updates include:
- adding a tool to extract the photoelectric absorption cross section data AND the real part of the complex conductivity for GERMANIUM;

In general, additional data or tools for other optical constants or target materials can also be added to this repository.

---

## Issue Tracking <a name="issue_tracking"></a>
For reporting issue with this package or requesting features, please submit a new issue for this project on GitLab. Unfortunately, merge requests are not currently supported for public users of this package. If you are a public user of this package and would like to submit a merge request, please contact the project manager.